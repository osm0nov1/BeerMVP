//
//  NetWorkAPI.swift
//  BeerMVP
//
//  Created by Rashit Osmonov on 28/6/22.
//

import Foundation

protocol NetworkService {
    func getBeerList(completion: @escaping ([Beer]) -> ())
    func getOneBeer(id: String, completion: @escaping ([Beer]) -> ())
    func searchBeer(id: Int, completion: @escaping ([Beer]) -> ())
    func getrandomBeer(completion: @escaping ([Beer]) -> ())
}

class NetworkApi: NetworkService {
   
    let baseURL = "https://api.punkapi.com/v2/beers"
    let session = URLSession.shared
    
    func getBeerList(completion: @escaping ([Beer]) -> ()) {
        let request = URLRequest(url: URL(string:"https://api.punkapi.com/v2/beers?page=1&per_page=30")!)
        let task = session.dataTask(with: request) { (data, _, _) in
            DispatchQueue.main.async {
                guard let data = data,
                      let response = try? JSONDecoder().decode([Beer].self, from: data) else {
                    completion([])
                    return
                }
                completion(response)
            }
        }
        task.resume()
    }
    
    func getOneBeer(id: String,completion: @escaping ([Beer]) -> ()) {
        let url = "\(baseURL)/\(id)"
        let request = URLRequest(url: URL(string: url)!)
        let task = session.dataTask(with: request) { (data, _, _) in
            DispatchQueue.main.async {
                guard let data = data,
                      let response = try? JSONDecoder().decode([Beer].self, from: data) else {
                    completion([])
                    return
                }
                completion(response)
            }
        }
        task.resume()
    }
    func searchBeer(id: Int, completion: @escaping ([Beer]) -> () ){
        let request = URLRequest(url: URL(string:"\(baseURL)?ids=\(id)")!)
        let task = session.dataTask(with: request) { (data, _, _) in
            DispatchQueue.main.async {
                guard let data = data,
                      let response = try? JSONDecoder().decode([Beer].self, from: data) else {
                    completion([])
                    return
                }
                completion(response)
            }
        }
        task.resume()
    }
    func getrandomBeer(completion: @escaping ([Beer]) -> () ){
       let url = ("\(baseURL)/random")
        let request = URLRequest(url: URL(string: url)!)
         let task = session.dataTask(with: request) { (data, _, _) in
            DispatchQueue.main.async {
                guard let data = data,
                      let response = try? JSONDecoder().decode([Beer].self, from: data) else {
                    completion([])
                    return
                }
                completion(response)
            }
        }
        task.resume()
    }
    
}
