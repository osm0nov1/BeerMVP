//
//  ViewController.swift
//  BeerMVP
//
//  Created by Rashit Osmonov on 28/6/22.
//
import UIKit
import SnapKit
protocol BeerListView: AnyObject {
    func presentBeerList()
}

class ViewController: UIViewController {
    
    var presenter: BeerListPresenter!
    
    private lazy var beerTable: UITableView = {
        let view = UITableView()
        view.delegate = self
        view.dataSource = self
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setSubviews()
        //        presenter = BeerListPresenter(view: self)
        presenter.viewDidLoad()
        presenter.presentBeers()
        
        view.backgroundColor = .white
    }
    
    private func setSubviews() {
        view.backgroundColor = .white
        title = "BeerList"
        view.addSubview(beerTable)
        beerTable.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.beersList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = BeerCell()
        let model = presenter.beersList[indexPath.row]
        cell.fill(model: model)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return view.bounds.height / 7
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let VC = DetailView(id: "\(presenter.beersList[indexPath.row].id!)")
        navigationController?.pushViewController(VC, animated: true)
    }
}

extension ViewController: BeerListView {
    func presentBeerList() {
        DispatchQueue.main.async {
            self.beerTable.reloadData()
        }
    }
}


