//
//  TabBarController.swift
//  BeerMVP
//
//  Created by Rashit Osmonov on 28/6/22.
//

import Foundation
import UIKit
class TabBarController: UITabBarController, UITabBarControllerDelegate {
    
    override func viewDidLoad() {
        delegate = self
        configureTabBarItems()
    }
    private func configureTabBarItems() {
        let listVC = ViewController()
        let listPresenter = BeerListPresenter(view: listVC)
        listVC.presenter = listPresenter
        listVC.tabBarItem = UITabBarItem(title: "BeerList", image: UIImage(systemName: "1.circle"), tag: 0)
        
        let searchVC = SearchVC()
        let SearchPresenter = SearchBeerPresenter(view: searchVC)
        searchVC.presenter = SearchPresenter
        searchVC.tabBarItem = UITabBarItem(title: "SEarch", image: UIImage(systemName: "2.circle"), tag: 1)
       
        let randomVC = RandomVC()
        let RandomPresenter = RandomPresenter(view: randomVC)
        randomVC.presenter = RandomPresenter
        randomVC.tabBarItem = UITabBarItem(title: "Random", image: UIImage(systemName: "3.circle" ), tag: 2)
        
        let listNavVC = UINavigationController(rootViewController: listVC)
        let searchNavVC = UINavigationController(rootViewController: searchVC)
        let randomNavVC = UINavigationController(rootViewController: randomVC)
        setViewControllers([listNavVC, searchNavVC, randomNavVC], animated: true)
    }
}

