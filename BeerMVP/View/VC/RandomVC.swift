//
//  RandomVC.swift
//  BeerMVP
//
//  Created by Rashit Osmonov on 29/6/22.
//

import Foundation
import SnapKit
import UIKit
protocol RandomBeerList: AnyObject {
    func randomBeer(beers: [Beer])
}

class RandomVC: UIViewController {
    
    private lazy var beerImage: UIImageView = {
            let view = UIImageView()
            view.contentMode = .scaleAspectFit
            return view
        }()
        
        private lazy var beerIdLabel: UILabel = {
            let view = UILabel()
            view.font = .systemFont(ofSize: 24, weight: .regular)
            view.textColor = .orange
            view.clipsToBounds = true
            return view
        }()
        
        private lazy var beerTitleLabel: UILabel = {
            let view = UILabel()
            view.font = .systemFont(ofSize: 14, weight: .bold)
            view.textColor = .black
            view.clipsToBounds = true
            return view
        }()
        
        private lazy var beerDescriptionLabel: UILabel = {
            let view = UILabel()
            view.font = .systemFont(ofSize: 14, weight: .regular)
            view.textColor = .black
            view.numberOfLines = 0
            return view
        }()
        
        private lazy var randomButton: UIButton = {
            let view = UIButton()
            view.setTitle("Random", for: .normal)
            view.addTarget(self, action: #selector(clickRandom(view:)), for: .touchUpInside)
            view.setTitleColor(UIColor.white, for: .normal)
            view.backgroundColor = .blue
            view.layer.cornerRadius = 3
            return view
        }()
        
        var presenter: RandomPresenter!
        
        override func viewDidLoad() {
            setupSubview()
        }
        
        @objc func clickRandom(view: UIButton) {
            presenter.random()
        }
    
        private func setupSubview() {
            view.backgroundColor = .white
            
            view.addSubview(beerImage)
            beerImage.snp.makeConstraints { make in
                make.centerX.equalTo(view.safeArea.centerX)
                make.top.equalTo(view.safeArea.top).offset(100)
                make.height.equalTo(view.frame.height / 3.3)
            }
            
            view.addSubview(beerIdLabel)
            beerIdLabel.snp.makeConstraints { make in
                make.centerX.equalTo(view.safeArea.centerX)
                make.top.equalTo(beerImage.snp.bottom).offset(10)
            }
            
            view.addSubview(beerTitleLabel)
            beerTitleLabel.snp.makeConstraints { make in
                make.centerX.equalTo(view.safeArea.centerX)
                make.top.equalTo(beerIdLabel.snp.bottom).offset(10)
            }
            
            view.addSubview(beerDescriptionLabel)
            beerDescriptionLabel.snp.makeConstraints { make in
                make.left.equalTo(view.safeArea.left).offset(13)
                make.right.equalTo(view.safeArea.right).offset(-10)
                make.top.equalTo(beerTitleLabel.snp.bottom).offset(10)
            }
            
            view.addSubview(randomButton)
            randomButton.snp.makeConstraints { make in
                make.bottom.equalToSuperview().offset(-100)
                make.leading.trailing.equalToSuperview().inset(100)
            }
        }
    }

extension RandomVC: RandomBeerList {
    func randomBeer(beers: [Beer]) {
        beerIdLabel.text = "\(beers[0].id!)"
        beerIdLabel.text = beers[0].name
        beerDescriptionLabel.text = beers[0].description
        guard let safeUrl = beers[0].imageURL else {return}
        beerImage.kf.setImage(with: URL(string: safeUrl))
    }
    
   
}
