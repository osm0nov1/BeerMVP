//
//  BeerCell.swift
//  BeerMVP
//
//  Created by Rashit Osmonov on 28/6/22.
//

import Foundation
import UIKit
import SnapKit
import Kingfisher

class BeerCell: UITableViewCell {
    
    private lazy var imageURL: UIImageView = {
        let view = UIImageView()
        view.contentMode = .scaleAspectFit
        return view
    }()
    private lazy var idLebel: UILabel = {
        let view = UILabel()
        view.textColor = .orange
        view.font = UIFont.systemFont(ofSize: 12, weight: .bold)
        view.text = "111"
        return view
    }()
    private lazy var titleLebel: UILabel = {
        let view = UILabel()
        view.textColor = .black
        view.font = UIFont.systemFont(ofSize: 18, weight: .bold)
        view.text = "Osmonov Rashit"
        return view
    }()
    private lazy var descriptionLebel: UILabel = {
        let view = UILabel()
        view.textColor = .black
        view.font = UIFont.systemFont(ofSize: 12, weight: .bold)
        view.text = "111"
        return view
    }()
    override func layoutSubviews() {
        
        addSubview(imageURL)
        imageURL.snp.makeConstraints { make in
            make.left.equalToSuperview().offset(15)
            make.top.equalToSuperview().offset(15)
            make.height.width.equalTo(self.bounds.height * 0.8)
        }
        addSubview(idLebel)
        idLebel.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(15)
            make.left.equalTo(imageURL.snp.right).offset(10)
            make.right.equalToSuperview().offset(-10)
            make.height.equalTo(15)
        }
        addSubview(titleLebel)
        titleLebel.snp.makeConstraints { make in
            make.top.equalTo(idLebel.snp.bottom).offset(3)
            make.left.equalTo(imageURL.snp.right).offset(10)
            make.right.equalToSuperview().offset(-10)
            make.height.equalTo(15)
            
        }
        addSubview(descriptionLebel)
        descriptionLebel.snp.makeConstraints { make in
            make.top.equalTo(titleLebel.snp.bottom)
            make.left.equalTo(imageURL.snp.right).offset(10)
            make.right.equalToSuperview().offset(-10)
            make.bottom.equalToSuperview()
        }
    }
    func fill(model: Beer) {
        imageURL.kf.setImage(with: URL(string: model.imageURL!))
        idLebel.text = "\(model.id!)"
        titleLebel.text = model.name
        descriptionLebel.text = model.description
    }
}
